# Fediverse NationStates region

Welcome to the
[Fediverse](https://www.nationstates.net/region=fediverse) I
guess. This repo will contain map-related stuff.

This uses [ICMMTS](https://codeberg.org/tuxcrafting/icmmts) to manage
map data.

## Contributing

To add your nation on there, add a file for your nation following the
format listed in the ICMMTS page, run `regen.sh` to regenerate the map
SVGs and check for conflicts, and send a pull request.
